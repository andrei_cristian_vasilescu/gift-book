const db = require('../firebase/firebaseConnection');
const randomString = require('randomstring');
const moment = require('moment');
const config = require('../config/config');

let tokensRef = db.ref("tokens");

const generateToken = (userId) => {
    let token = randomString.generate(50);
    tokensRef.orderByChild("userId").equalTo(userId).once("value")
        .then(snapshot => {
            let tokensToRemove = [];
            snapshot.forEach( element => {
                tokensToRemove.push(element.key);
            })
            tokensToRemove.forEach(token => {
                db.ref(`tokens/${token}`).remove();
            })
        })
        .then(()=> {
            tokensRef.push({
                userId: userId,
                token: token,
                expirationDate: moment().add(config.TOKEN_EXPIRATION_DAYS, 'd').valueOf()
            });
        })
    return token;
}

module.exports = {
    generateToken: generateToken,
}