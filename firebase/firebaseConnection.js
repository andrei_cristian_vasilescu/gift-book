var admin = require("firebase-admin");
var serviceAccount = require("../config/firebaseServiceAccountKey.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://gift-book.firebaseio.com"
});

var db = admin.database();

module.exports = db;