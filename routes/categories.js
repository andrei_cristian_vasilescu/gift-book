const express = require('express');
const router = express.Router();
const db = require('../firebase/firebaseConnection');

const categoriesRef = db.ref("categories");

router.get("/", (req, res, next) => {
    categoriesRef.once("value")
        .then(snapshot => {
            let categories = [];
            snapshot.forEach(element=>{
                let category = element.val();
                category.id = element.key;
                categories.push(category);
            });
            res.status(200).json(categories);
        })
        .catch(err=>next(err, req, res));

});

router.post("/", (req, res, next) => {
    let newCategory = req.body;

    if(! newCategory.name) {
        res.status(400).send("Numele categoriei este obligatoriu")
    } else {
        categoriesRef.orderByChild("name").equalTo(newCategory.name).once("value")
        .then(snapshot=> {
            let categories = [];
            snapshot.forEach(element=>categories.push(element.val()));
            if(categories.length === 0) {
                categoriesRef.push({
                    name: newCategory.name,
                    description: newCategory.description
                });
                res.status(201).send("Categoria a fost adaugata cu succes");
            } else {
                res.status(409).send("O categorie cu acest nume exista");
            }
        })
        .catch(()=>{
            next();
        }) 
    }

   
});

router.delete("/:id", (req, res, next) => {
    db.ref(`categories/${req.params.id}`).once("value")
        .then(snapshot => {
            let category = snapshot.val();
            if(category) {
                db.ref("books").orderByChild("categoryId").equalTo(req.params.id).once("value")
                    .then(snapshot=>{
                        let books = [];
                        snapshot.forEach(element => {
                            let book = element.val();
                            books.push(book);
                        }) 
                        if(books.length === 0) {
                            db.ref(`categories/${req.params.id}`).remove()
                                .then(()=>{
                                    res.status(200).send("Categoria a fost stearsa cu succes")
                                })
                                .catch(err=>{
                                    next(err, req, res)
                                })
                        } else {
                            res.status(409).send("Categoria nu poate fii stearsa. Este utilizata de cartile existente")
                        }
                    })
                
            } else {
                res.status(404).send("Categoria nu exista")
            }
        })
        .catch(()=>{
            next();
        })
});

router.put("/:id", (req, res, next) => {

    let newCategory = req.body;

    if(!newCategory.name) {
        res.status(400).send("Numele categoriei este obligatoriu")
    }

    db.ref(`categories/${req.params.id}`).once("value")
        .then(snapshot => {
            let category = snapshot.val();
            category.id = snapshot.key
            if(category) {

                categoriesRef.orderByChild("name").equalTo(newCategory.name).once("value")
                    .then(snapshot=> {
                        let categories = [];
                        snapshot.forEach(element=>{
                            if(element.key !== category.id) {
                                categories.push(element.val())
                            }
                        });
                        if(categories.length === 0) {
                            db.ref(`categories/${req.params.id}`).set({
                                name: newCategory.name,
                                description: newCategory.description
                            });
                            res.status(200).send("Categoria a fost actualizata cu succes")
                        } else {
                            res.status(409).send("Exista deja o categorie cu acest nume");
                        }
                    })
                    .catch(()=>{
                        next();
                    }) 

               
            } else {
                res.status(404).send("Categoria nu exista")
            }
        })
        .catch(()=>{
            next();
        })
})

module.exports = router;