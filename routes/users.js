const express = require('express');
const router = express.Router();
const db = require('../firebase/firebaseConnection');
const authenticathor = require('../security/authenticathor')
const config = require("../config/config");

const usersRef = db.ref("users");

let token = config.SECURITY_TOKEN;
var moment = require("moment");
var md5 = require("md5");

var log4js = require('log4js');
var logger = log4js.getLogger("users");
logger.level = 'info';

router.post("/register", (req, res, next) => {
    let userToRegister = req.body;
    if(!userToRegister.email) {
        res.status(400).send("Emailul este obligatoriu");
    } else if(!userToRegister.password) {
        res.status(400).send("Parola este obligatorie");
    } else if(!userToRegister.firstName){
        res.status(400).send("Prenumele este obligatoriu");
    } else if(!userToRegister.lastName){
        res.status(400).send("Numele este obligatoriu");
    } else if(!userToRegister.phone){
        res.status(400).send("Numarul de telefon este obligatoriu");
    } else {
        usersRef.orderByChild("email").equalTo(userToRegister.email).once("value")
            .then(snapshot=>{
                let users = [];
                snapshot.forEach( element => users.push(element.val()));
                if(users.length !== 0) {
                    res.status(409).send("Un utilizator cu acest email exista deja");
                } else {
                    userToRegister.registrationDate = moment().valueOf();
                    let encryptedPassword = md5(userToRegister.password + token + userToRegister.registrationDate)
                    userToRegister.password = encryptedPassword;
                    let user = {
                        firstName: userToRegister.firstName,
                        lastName: userToRegister.lastName,
                        email: userToRegister.email,
                        password: encryptedPassword,
                        phone: userToRegister.phone,
                        registrationDate: userToRegister.registrationDate,
                        address: {
                            country: userToRegister.address.country,
                            city: userToRegister.address.city,
                            street: userToRegister.address.street,
                        }
                    }
                    usersRef.push(user);
                    res.send("Utilizatorul a fost inregistrat cu succes")
                }
            })
            .catch(err=>next(err, req, res));
    }
   
});

router.post("/login", (req, res, next) => {
    let credentials = req.body;
    logger.info("Received login request for user:", credentials.email)
    usersRef.orderByChild("email").equalTo(credentials.email).once("value")
        .then(snapshot=>{
            let users = [];
            snapshot.forEach( element => {
                let user = element.val();
                user.id = element.key;
                users.push(user);
            });
            if(users.length === 0) {
                res.status(401).send("Emailul sau parola sunt incorecte");
            } else {
                let user = users[0];
                let password = md5(credentials.password + token + user.registrationDate);
                if(password === user.password) {
                    delete user.password;
                    delete user.registrationDate;
                    user.token = authenticathor.generateToken(user.id);
                    res.status(200).json(user);
                } else {
                    res.status(401).send("Emailul sau parola sunt incorecte");
                }
            }
        })
        .catch( err => next(err, req, res));
});

router.get("/api/users", (req, res, next) => {
    usersRef.once("value")
        .then(snapshot=> {
            let users = [];
            snapshot.forEach( element => {
                let user = element.val();
                user.id = element.key;
                delete user.password
                delete user.registrationDate;
                users.push(user);
            });
            res.status(200).json(users);
        })
        .catch(err=>next(err, req, res));
})


module.exports = router;