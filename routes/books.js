const express = require('express');
const router = express.Router();
const db = require('../firebase/firebaseConnection');
const moment = require("moment");

const booksRef = db.ref("books");

router.get("/",(req, res, next) => {
    booksRef.once("value")
        .then( (snapshot) => {
            let books = [];
            snapshot.forEach((element) => {
                let book = element.val();
                book.id = element.key;
                books.push(book);
            })
            res.status(200).json(books);
        })
        .catch((err)=>{
            next(err, req, res);
        })
});


router.post("/",(req, res, next) => {
    let newBook = req.body;
    if(!newBook.name) {
        res.status(400).send("Numele cartii este obligatoriu");
    } 
    else if(!newBook.categoryId) {
        res.status(400).send("Categoria este oligatorie");
    }
    else if(!newBook.userId) {
        res.status(400).send("Utilizatorul care adauga cartea este obligatoriu");
    } else {
        db.ref(`categories/${newBook.categoryId}`).once("value")
            .then(snapshot => {
                let category = snapshot.val();
                if(category) {
                    db.ref(`users/${newBook.userId}`).once("value")
                        .then(snapshot => {
                            let user = snapshot.val();
                            if(user) {
                                booksRef.push({
                                    name: newBook.name,
                                    description: newBook.description,
                                    categoryId: newBook.categoryId,
                                    userId: newBook.userId,
                                    author: newBook.author,
                                    language: newBook.language,
                                    isbn: newBook.isbn,
                                    year: newBook.year,
                                    pages: newBook.pages,
                                    date: moment().valueOf()
                                });
                                res.status(201).send(`Cartea a fost adaugata cu success`);
                            } else {
                                res.status(400).send(`Utilizatorul cu id-ul ${newBook.userId} nu exista`);
                            }
                        })
                        .catch(err=>next(err, req, res));

                } else {
                    res.status(400).send(`Categoria cu id-ul ${newBook.categoryId} nu exista`);
                }
            })
            .catch(err=>{
                next(err, req, res);
            })
        }
})

router.delete("/:id", (req, res, next) => {
    db.ref(`books/${req.params.id}`).once("value")
        .then(snapshot=>{
            let book = snapshot.val();
            if(book) {
                db.ref(`books/${req.params.id}`).remove()
                    .then(()=>{
                        res.status(200).send("Cartea a fost stearsa");
                    }) 
                    .catch((err)=>{
                        next(err, req, res)
                    })
            } else {
                res.status(404).send(`Cartea cu id-ul ${req.params.id} nu exista`)
            }
        })
        .catch((err)=>{
            next(err, req, res)
        })
});

router.put("/:id", (req, res, next) => {
    let newBook = req.body;
    if(!newBook.name) {
        res.status(400).send("Numele cartii este obligatoriu");
    } 
    else if(!newBook.categoryId) {
        res.status(400).send("Categoria este oligatorie");
    }
    else if(!newBook.userId) {
        res.status(400).send("Utilizatorul care adauga cartea este obligatoriu");
    } else {
        db.ref(`books/${req.params.id}`).once("value")
            .then(snapshot=>{
                let dbBook = snapshot.val();
                if(dbBook) {
                    db.ref(`categories/${newBook.categoryId}`).once("value")
                        .then(snapshot => {
                            let category = snapshot.val();
                            if(category) {
                                db.ref(`users/${newBook.userId}`).once("value")
                                    .then(snapshot => {
                                        let user = snapshot.val();
                                        if(user) {
                                            db.ref(`books/${req.params.id}`).set({
                                                name: newBook.name,
                                                description: newBook.description,
                                                categoryId: newBook.categoryId,
                                                userId: newBook.userId,
                                                author: newBook.author,
                                                language: newBook.language,
                                                isbn: newBook.isbn,
                                                year: newBook.year,
                                                pages: newBook.pages,
                                                date: dbBook.date
                                            })
                                            .then(()=>{
                                                res.status(201).send(`Cartea a fost actualizata cu success`);
                                            })
                                            .catch((err)=> {
                                                next(err, req, res);
                                            })
                                        } else {
                                            res.status(400).send(`Utilizatorul cu id-ul ${newBook.userId} nu exista`);
                                        }
                                    })
                                    .catch(err=>next(err, req, res));

                            } else {
                                res.status(400).send(`Categoria cu id-ul ${newBook.categoryId} nu exista`);
                            }
                        })
                        .catch(err=>{
                            next(err, req, res);
                        })


                } else {
                    res.status(404).send(`Cartea cu id-ul ${req.params.id} nu exista`)
                }
            })
            .catch((err)=>{
                next(err, req, res)
            })
    }
})

module.exports = router;
