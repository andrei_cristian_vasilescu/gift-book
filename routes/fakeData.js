const express = require('express');
const router = express.Router();
const db = require('../firebase/firebaseConnection');
const faker = require("faker");
const config = require("../config/config");

let token = config.SECURITY_TOKEN;
var moment = require("moment");
var md5 = require("md5");

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

router.get("/categories", (req, res) => {
    for(let i=0;i<20;i++) {
        let categoryName = faker.fake("{{random.word}}");
        let category = {
            name: categoryName,
            description: categoryName
        };
        db.ref("categories").push(category);
    }
    res.send("Fake");
})


router.get("/users", (req, res) => {
    for(let i=0;i<100;i++) {
        let registrationDate = moment().valueOf();
        let user = {
            firstName: faker.fake("{{name.firstName}}"),
            lastName: faker.fake("{{name.lastName}}"),
            email: faker.fake("{{internet.email}}"),
            password: md5("123456789" + token + registrationDate),
            registrationDate: registrationDate,
            phone: faker.fake("{{phone.phoneNumber}}"),
            address: {
                country: faker.fake("{{address.country}}"),
                city: faker.fake("{{address.city}}"),
                street: faker.fake("{{address.streetName}}")
            }
        }
        db.ref("users").push(user);
    }
    res.send("Fake");
});

router.get("/books", (req, res) => {
    let categories = [];
    db.ref("categories").once("value")
        .then(snapshot=> {
            snapshot.forEach(element=> {
                let category = element.val();
                category.id = element.key;
                categories.push(category);
            })
            let users = [];
            db.ref("users").once("value")
                .then(snapshot => {
                    snapshot.forEach(element => {
                        let user = element.val();
                        user.id = element.key;
                        users.push(user);
                    })
                    for(let i=0;i<50;i++) {
                        let book = {
                            name: capitalizeFirstLetter(faker.fake("{{random.words}}")),
                            author: faker.fake("{{name.firstName}} {{name.lastName}}"),
                            language: "English",
                            isbn: faker.fake("{{finance.iban}}"),
                            year: 1918 + parseInt(Math.random() * 100),
                            pages: parseInt(Math.random() * 1000),
                            description: faker.fake("{{lorem.paragraphs}} {{lorem.paragraphs}} {{lorem.paragraphs}}"),
                            date: moment().valueOf(),
                            categoryId: categories[Math.floor(Math.random() * (categories.length))].id,
                            userId: users[Math.floor(Math.random() * (users.length))].id
                        }
                        db.ref("books").push(book);
                    }
                })
        })
        res.send("Fake");

});


module.exports = router;