var express = require("express");
var bodyParser = require("body-parser");
var cors = require("cors");
const path = require('path');


const db = require('./firebase/firebaseConnection');
const moment = require('moment');

var app = express();
app.use(bodyParser.json());
app.use(cors());
app.use(express.static(path.join(__dirname, 'static')));


var log4js = require('log4js');
var logger = log4js.getLogger("app");
logger.level = 'info';

const users = require('./routes/users');
const categories = require('./routes/categories');
const books = require('./routes/books');
const fakeData = require('./routes/fakeData');


app.use((req,res,next) => {
    let method = req.method;
    if( req.url.split('/')[1] === "api" && (method === 'POST' || method === 'PUT' || method === 'DELETE')) {
        let bearer = req.headers['authorization'];
        if(!bearer) {
            logger.info("Unauthorized request. Bearer token is required");
            res.status(401).send("Unauthorized")
        } else {
            let token = bearer.split(" ")[1].trim();
            db.ref("tokens").orderByChild("token").equalTo(token).once("value")
                .then(snapshot => {
                    let dbTokens = [];
                    snapshot.forEach(element => dbTokens.push(element.val()));
                    let dbToken = dbTokens[0];
                    if(dbToken && dbToken.token === token && moment().valueOf() < dbToken.expirationDate) {
                        if(req.url.split('/')[2] === 'categories') {
                            let userId = dbToken.userId;
                            db.ref(`users/${userId}`).once("value")
                                .then(snapshot=>{
                                    let user = snapshot.val();
                                    if(user && user.role === 'admin') {
                                        next();
                                    } else {
                                        logger.info("Unauthorized request. Admin role is needed for add/update/delete categories");
                                        res.status(401).send("Unauthorized")
                                    }
                                })
                        } else {
                            next();
                        }
                    } else {
                        logger.info("Unauthorized request. Invalid token: " + token + " or token is expired");
                        res.status(401).send("Unauthorized")
                    }
                })
        }
       
    }  else {
        next();
    }
})

app.use('/', users);
app.use('/api/categories', categories);
app.use('/api/books', books);
app.use('/api/fake', fakeData);


app.use((err, req, res) => {
    logger.error("Server error", err);
    res.status(500).send("Server error");
});

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'static/index.html'))
  });

app.listen(8080, () => {
    logger.info("Server ready on port 8080");
});